# Libraries import 
import requests
from urllib.parse import urljoin
import bs4
# Crawling urls
class Crawler:
    def __init__(self, max_pages, urls=[], verify=[]):
        self.max_pages = max_pages
        self.verify = verify
        self.urls_to_visit = urls
        self.visited_urls = []

    def valid_url(self, url, verify):
        r = requests.get(url, verify=False)
        if r.status_code == 200:
            #print(r.status_code)
            soup = bs4.BeautifulSoup(r.content, "html.parser")
            return soup
        else:
            #print(f"error {r.status_code}.")
            return None
#
    def get_internal_urls(self, url, verify):
        html = self.valid_url(url, verify=False)

        for link in html.find_all('a'):
            if 'href' in link.attrs:
                    if "http" in link.attrs['href']:
                        internal_link = link.attrs["href"]
                        self.add_urls_to_visit(internal_link)
                    else:
                        if link.attrs['href'].startswith('/'):
                            internal_link = urljoin(url, link.attrs['href'])

        print(self.urls_to_visit)

    def add_urls_to_visit(self, internal_link):
        if internal_link not in self.urls_to_visit and internal_link not in self.visited_urls:
            self.urls_to_visit.append(internal_link)

data_sources = ["https://data.ansm.sante.fr/specialite/63003440","https://data.ansm.sante.fr/specialite/61639048","https://data.ansm.sante.fr/specialite/65213665"]
for i in data_sources:
    Crawler(max_pages=3).get_internal_urls(i, verify=False)
