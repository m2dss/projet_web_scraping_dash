# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore")

import pandas as pd

# Chargement des fichiers

Patients_traites = pd.read_csv('D:/M2DSS/Projets_python/Data_dash/Patients_traites.csv', sep = ';')
Declarants_EI = pd.read_csv('D:/M2DSS/Projets_python/Data_dash/Declarants_EI.csv', sep = ';')
Patients_effets_indesirables = pd.read_csv('D:/M2DSS/Projets_python/Data_dash/Patients_effets_indesirables.csv', sep = ';')
Organes_affectes_EI = pd.read_csv('D:/M2DSS/Projets_python/Data_dash/Organes_affectes_EI.csv', sep = ';')

List_declarant = ['Médecin', 'Pharmacien', 'Autre-professionnel-de-santé', 'Patient-Autre-non-professionnel de santé']

Organes_affectes_EI_propra = Organes_affectes_EI[['organes_affectes', 'nb_declarations_propra']]
Organes_affectes_EI_carve = Organes_affectes_EI[['organes_affectes', 'nb_declarations_carve']]
Organes_affectes_EI_bisop = Organes_affectes_EI[['organes_affectes', 'nb_declarations_bisop']]

type_declarant_propra = Declarants_EI[['categorie', 'propranolol']]
type_declarant_carve = Declarants_EI[['categorie', 'carvedilol']]
type_declarant_bisop = Declarants_EI[['categorie', 'bisoprolol']]

total_patients_p = Patients_traites['nb_patients_traites_an'][0]
total_patients_c = Patients_traites['nb_patients_traites_an'][1]
total_patients_b = Patients_traites['nb_patients_traites_an'][2]

total_ei_p = Patients_effets_indesirables['nb_decla_ei'][0]
total_ei_c = Patients_effets_indesirables['nb_decla_ei'][1]
total_ei_b = Patients_effets_indesirables['nb_decla_ei'][2]


import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go


app = dash.Dash(__name__, assets_folder = 'assets')
server = app.server

app.layout = html.Div(
    html.Div(
        children = [
            html.Img(src=app.get_asset_url('Pills-Medicine-Transparent.png'), 
                             style={'display' : 'block', 'width': '150px', 'height': 'center', 'margin-top': '10px'}),
            html.H1("EFFETS INDESIRABLES DES MEDICAMENTS ASSOCIES AUX MALADIES CARDIOVASCULAIRES", 
                    style = {'text-align': 'center', 'fontSize': '20px'}),
            
            html.Div([
                html.H1('Sur la période 2014 - 2021', style={'margin-top': '-8px', 'text-align': 'center', 'fontSize': '20px'})
            ]),
            
            html.Div(
                children = [
                    html.H1(
                        children='Noms des Medicaments',
                        style={'margin-top': '-8px', 'textalign': 'center', 'fontSize': '20px'}
                    ),
                    
                    # Choix du médicament
                    html.Div([
                        dcc.Dropdown(
                            id='Nom_Medicament',
                            options=[
                                {'label': 'Propranolol arrow 40 mg', 'value': 'Propranolol'},
                                {'label': 'Carvedilol arrow 12,5 mg', 'value': 'Carvedilol'},
                                {'label': 'Bisoprolol quiver lab 10 mg', 'value': 'Bisoprolol'}
                            ],
                            value='Propranolol'
                        ),
                    ], style={'width': '300px'}),
                    
                    html.Div([
                        
                            html.Div([
                                html.P('Total des patients traités en ville', className='text-primary-p1',
                                       style={'textAlign': 'center', 'fontSize': '20px'}),
                                html.Div(id='stats_patients_traites', className='font-bold text-title',
                                         style={'textAlign': 'center', 'margin': 'auto', 'font-weight': 'bold', 'color': 'darkcyan', 
                                                'border': '3px solid black', 'padding': '5px', 'width': '300px', 'fontsize' : '35px'}),
                                ], className='row'),
                                
                            html.Div([
                                html.P('Total des déclarations d\'éffets indésirables', className='text-primary-p1',
                                       style={'textAlign': 'center', 'fontSize': '20px'}),
                                html.Div(id='stats_EI', className='font-bold text-title',
                                         style={'textAlign': 'center', 'margin': 'auto', 'font-weight': 'bold', 'color': 'darkcyan', 
                                                'border': '3px solid black', 'padding': '5px', 'width': '300px', 'fontsize' : '35px'}),
                            ], className='row')
                        
                    ], className='flex-display'),
                    
                    html.Div([
                        
                        # # Effets indésirables par système d’organes
                        html.Div([
                            dcc.Graph(id='graph-organes-affectes')
                        ],  className='row flex-display'),
                        
                        # Répartition des patients traités par sexe
                        html.Div([
                            dcc.Graph(id='pie-chart-sex', className='pie-chart')
                        ],  className='row'),
                        
                        # Répartition des patients traités par classe d'âge
                        html.Div([
                            dcc.Graph(id='pie-chart-age', className='pie-chart')
                        ],  className='row'),
                        
                        # Répartition par sexe des patients traités parmi les déclarations d'effets indésirables 
                        html.Div([
                            dcc.Graph(id='pie-chart-sex-non-traites', className='pie-chart')
                        ],  className='row'),
                        
                        # Répartition par âge des patients traités parmi les déclarations d'effets indésirables
                        html.Div([
                            dcc.Graph(id='pie-chart-age-non-traites', className='pie-chart')
                        ],  className='row'),
                        
                        # Répartition par type de déclarant
                        html.Div([
                            dcc.Graph(id='graph-type-declarants')
                        ],  className='row flex-display')

                           
                    ]),
                    
                    
                ]
            )
        ]
    )
)



# Fonctions pour affecter les données au médicament choisi

# 1 - Total patients traités  
def load_stats_pat_traites(Medicament):
    if Medicament == 'Propranolol':
        return total_patients_p
    elif Medicament == 'Carvedilol':
        return total_patients_c
    elif Medicament == 'Bisoprolol':
        return total_patients_b
    else:
        return None

# 2 - Total déclarations des effets inddésirables
def load_stats_total_EI(Medicament):
    if Medicament == 'Propranolol':
        return total_ei_p
    elif Medicament == 'Carvedilol':
        return total_ei_c
    elif Medicament == 'Bisoprolol':
        return total_ei_b
    else:
        return None

# 1 - Déclarants
def load_data_declarant(Medicament):
    if Medicament == 'Propranolol':
        return type_declarant_propra
    elif Medicament == 'Carvedilol':
        return type_declarant_carve
    elif Medicament == 'Bisoprolol':
        return type_declarant_bisop
    else:
        return None

# 2- Effets indésirables
def load_data_org_aff(Medicament):
    if Medicament == 'Propranolol':
        return Organes_affectes_EI_propra
    elif Medicament == 'Carvedilol':
        return Organes_affectes_EI_carve
    elif Medicament == 'Bisoprolol':
        return Organes_affectes_EI_bisop
    else:
        return None

        
# 1
@app.callback(
    Output('stats_patients_traites', 'children'),
    [Input('Nom_Medicament', 'value')]
)

def update_stats(Medicament):
    nb_patients_traites = load_stats_pat_traites(Medicament)
    
    nb_patients_traites = Patients_traites['nb_patients_traites_an'][0] if Medicament == 'Propranolol' else Patients_traites['nb_patients_traites_an'][1] if Medicament == 'Carvedilol' else Patients_traites['nb_patients_traites_an'][2],
    
    return nb_patients_traites

# 2
@app.callback(
    Output('stats_EI', 'children'),
    [Input('Nom_Medicament', 'value')]
)

def update_stats_EI(Medicament):
    nb_decl_ei = load_stats_total_EI(Medicament)
    
    nb_decl_ei = Patients_effets_indesirables['nb_decla_ei'][0] if Medicament == 'Propranolol' else Patients_effets_indesirables['nb_decla_ei'][1] if Medicament == 'Carvedilol' else Patients_effets_indesirables['nb_decla_ei'][2],
    
    return nb_decl_ei

# 3
@app.callback(
    Output('graph-type-declarants', 'figure'),
    [Input('Nom_Medicament', 'value')]
)

def update_declarants_graph(Medicament):
    
    Declarants_EI = load_data_declarant(Medicament)
    
    figure_decl = {
        'data': [
            {'x': List_declarant, 
             'y': Declarants_EI['propranolol'] if Medicament == 'Propranolol' else Declarants_EI['carvedilol'] if Medicament == 'Carvedilol' else Declarants_EI['bisoprolol'],  
             'type': 'bar', 'marker': {'color': ['darkcyan', 'steelblue', 'mediumorchid', 'gold']}
             }
        ],
        'layout': {
            'title': 'Répartitions par type de déclarant',
            'xaxis': {
                'title': 'Déclarant',
                'tickangle': -45,  # Rotation des étiquettes de l'axe des abscisses
                'automargin': True  # Ajustement automatique des marges de l'axe des abscisses
            },
            'yaxis': {
                'title': 'Nombre de déclarations',
                'automargin': True  # Ajustement automatique des marges de l'axe des ordonnées
            },
            'bargap': 0.2,
            'height': 600,  # Hauteur de la figure en pixels
            'width': 800,  # Largeur de la figure en pixels
            'margin': {'l': 100, 'r': 100, 't': 100, 'b': 100}  # Marges de la figure
        }
    }


    return figure_decl


# 4
@app.callback(
    Output('graph-organes-affectes', 'figure'),
    [Input('Nom_Medicament', 'value')]
)

def update_figure(Medicament):
    
    Organes_affectes_EI = load_data_org_aff(Medicament)
    if Organes_affectes_EI is None:
        return {}
    
    figure_EI = {
        'data': [
            {'x': Organes_affectes_EI['organes_affectes'], 
             'y': Organes_affectes_EI['nb_declarations_propra'] if Medicament == 'Propranolol' else Organes_affectes_EI['nb_declarations_carve'] if Medicament == 'Carvedilol' else Organes_affectes_EI['nb_declarations_bisop'],  
             'type': 'bar'}
        ],
        'layout': {
            'title': 'Nombre de déclarations d’Effets indésirables par système d’organes',
            'xaxis': {
                'title': 'Organes affectés',
                'tickangle': -45,  # Rotation des étiquettes de l'axe des abscisses
                'automargin': True  # Ajustement automatique des marges de l'axe des abscisses
            },
            'yaxis': {
                'title': 'Nombre de déclarations',
                'automargin': True  # Ajustement automatique des marges de l'axe des ordonnées
            },
            'height': 600,  # Hauteur de la figure en pixels
            'width': 1200,  # Largeur de la figure en pixels
            'margin': {'l': 100, 'r': 100, 't': 100, 'b': 100},
            # Marges de la figure
        }
    }


    return figure_EI

 
@app.callback(
    [Output('pie-chart-sex', 'figure'), Output('pie-chart-age', 'figure'), Output('pie-chart-sex-non-traites', 'figure'), Output('pie-chart-age-non-traites', 'figure')],
    [Input('Nom_Medicament', 'value')]
)
def update_pie_chart(selected_medication):
    # Obtenir les pourcentages d'hommes et de femmes pour le médicament sélectionné
    # Remplacez les noms de colonnes par les noms réels dans votre dataframe
    male_percentage = 0
    female_percentage = 0
    age_group1_percentage = 0
    age_group2_percentage = 0
    age_group3_percentage = 0
    male_non_traites_percentage = 0
    female_non_traites_percentage =0
    age_group1_non_traites_percentage = 0
    i = 0
    if selected_medication == "Propranolol":
        i = 0
    elif selected_medication == "Carvedilol":
        i = 1
    elif selected_medication == "Bisoprolol":
        i = 2
    male_percentage = Patients_traites['hommes'][i]  # pourcentage d'hommes pour le médicament sélectionné
    female_percentage = Patients_traites['femmes'][i]  # pourcentage de femmes pour le médicament sélectionné
    male_non_traites_percentage = Patients_effets_indesirables['hommes'][i]
    female_non_traites_percentage =Patients_effets_indesirables['femmes'][i]
    # Obtenir les pourcentages par tranche d'âge pour le médicament sélectionné
    # Remplacez les noms de colonnes par les noms réels dans votre dataframe
    age_group1_percentage = Patients_traites['0-19 ans'][i]  # pourcentage pour la tranche d'âge 0-19 ans
    age_group2_percentage = Patients_traites['20-59 ans'][i]  # pourcentage pour la tranche d'âge 20-59 ans
    age_group3_percentage = Patients_traites['60 ans et plus'][i]  # pourcentage pour la tranche d'âge 60 ans et plus
    age_group1_non_traites_percentage= Patients_effets_indesirables['0-19 ans'][i]
    age_group2_non_traites_percentage = Patients_effets_indesirables['20-59 ans'][i]
    age_group3_non_traites_percentage = Patients_effets_indesirables['60 ans et plus'][i]
    # Créer le graphique camembert pour la répartition par sexe
    data_sex = [
        go.Pie(
            labels=['Hommes', 'Femmes'],
            values=[male_percentage, female_percentage],
            hole=0.5,
            marker=dict(colors=['#1f77b4', '#ff7f0e']),
            hoverinfo='label+percent',
            textinfo='value',
            textfont=dict(size=16),
            showlegend=True
        )
    ]

    layout_sex = go.Layout(
        annotations=[dict(text='Répartition des patients traités par sexe', x=1, y=1.5, showarrow=False, font=dict(size=16))],
        margin=dict(l=1500, r=20, t=80, b=20),
        height=200
        
    )

    # Créer le graphique camembert pour la répartition par tranche d'âge
    data_age = [
        go.Pie(
            labels=['0-19 ans', '20-59 ans', '60 ans et plus'],
            values=[age_group1_percentage, age_group2_percentage, age_group3_percentage],
            hole=0.5,
            marker=dict(colors=['#2ca02c', '#d62728', '#9467bd']),
            hoverinfo='label+percent',
            textinfo='value',
            textfont=dict(size=16),
            showlegend=True
        )
    ]

    layout_age = go.Layout(
        annotations=[dict(text='Répartition des patients traités par classe d\'âge', x=1, y=1.5, showarrow=False, font=dict(size=16))],
        margin=dict(l=1500, r=20, t=80, b=20),
        height=200
    )

        # Créer le graphique camembert pour la répartition par sexe des patients non traités
    data_sex_non_traites = [
        go.Pie(
            labels=['Hommes', 'Femmes'],
            values=[male_non_traites_percentage, female_non_traites_percentage],
            hole=0.5,
            marker=dict(colors=['#1f77b4', '#ff7f0e']),
            hoverinfo='label+percent',
            textinfo='value',
            textfont=dict(size=16),
            showlegend=True
        )
    ]

    layout_sex_non_traites = go.Layout(
        annotations=[dict(text='Répartition par sexe des patients traités parmi les déclarations d\'effets indésirables ', x=1, y=1.5, showarrow=False, font=dict(size=16))],
        margin=dict(l=1500, r=20, t=80, b=20),
        height=200
    )

        # Créer le graphique camembert pour la répartition par tranche d'âge des patients non traités
    data_age_non_traites = [
        go.Pie(
            labels=['0-19 ans', '20-59 ans', '60 ans et plus'],
            values=[age_group1_non_traites_percentage, age_group2_non_traites_percentage, age_group3_non_traites_percentage],
            hole=0.5,
            marker=dict(colors=['#2ca02c', '#d62728', '#9467bd']),
            hoverinfo='label+percent',
            textinfo='value',
            textfont=dict(size=16),
            showlegend=True
        )
    ]

    layout_age_non_traites = go.Layout(
        annotations=[dict(text='Répartition par âge des patients traités parmi les déclarations d\'effets indésirables',x=1, y=1.5, showarrow=False, font=dict(size=16))],
        margin=dict(l=1500, r=20, t=80, b=20),
        height=200
    )

    return {'data': data_sex, 'layout': layout_sex}, {'data': data_age, 'layout': layout_age}, {'data': data_sex_non_traites, 'layout': layout_sex_non_traites}, {'data': data_age_non_traites, 'layout': layout_age_non_traites}
   
    

if __name__ == "__main__" :
    app.run_server(debug = True)